import 'package:flutter/material.dart';
import 'package:uts_mobile/hasil.dart';
import 'package:uts_mobile/profil.dart';

class LuasPersegi extends StatefulWidget {
  @override
  _LuasPersegiState createState() => _LuasPersegiState();
}

class _LuasPersegiState extends State<LuasPersegi> {
  int panjang = 0;
  int lebar = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Luas Persegi'),
            backgroundColor: Colors.blue,
            actions: <Widget>[
              Container(
                width: 50,
                height: 50,
                decoration:
                    BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                child: Center(
                  child: IconButton(
                      icon: const Icon(Icons.person),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profil()));
                      }),
                ),
              )
            ],
          ),
          body: ListView(
            children: <Widget>[
              new Container(
                  padding: new EdgeInsets.all(20.0),
                  child: new Column(
                    children: <Widget>[
                      Text(
                        'Kalkulator Luas Persegi',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new Padding(padding: new EdgeInsets.all(20.0)),
                      new Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              onChanged: (txt) {
                                setState(() {
                                  panjang = int.parse(txt);
                                });
                              },
                              keyboardType: TextInputType.number,
                              maxLength: 3,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 16,
                              ),
                              decoration: InputDecoration(
                                  labelText: "Panjang Persegi",
                                  suffix: Text('cm'),
                                  border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0)),
                                  //filled: true,
                                  hintText: 'Panjang'),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: TextField(
                              onChanged: (txt) {
                                setState(() {
                                  lebar = int.parse(txt);
                                });
                              },
                              keyboardType: TextInputType.number,
                              maxLength: 3,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 16,
                              ),
                              decoration: InputDecoration(
                                  labelText: "Lebar Persegi",
                                  suffix: Text('cm'),
                                  border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0)),
                                  //filled: true,
                                  hintText: 'Lebar'),
                            ),
                          ),
                        ],
                      ),
                      new Padding(padding: new EdgeInsets.only(top: 20.0)),
                      Container(
                        //height: double.infinity,
                        margin:
                            EdgeInsets.only(left: 10, right: 10, bottom: 20),
                        // ignore: deprecated_member_use
                        child: RaisedButton(
                          onPressed: () {
                            var route = new MaterialPageRoute(
                              // ignore: non_constant_identifier_names
                              builder: (BuildContext) => new Hasil(
                                panjang_persegi: panjang,
                                lebar_persegi: lebar,
                              ),
                            );
                            Navigator.of(context).push(route);
                            /* Navigator.push(
                        context,
                        MaterialPageRoute(builder:  (context)  => BMIResult(tinggi_badan: tinggi, berat_badan: berat)),
                        );
                        */
                          },
                          padding: EdgeInsets.all(10.0),
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: Text(
                            'Hitung',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ))
            ],
          )),
    );
  }
}
