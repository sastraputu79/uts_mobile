import 'package:flutter/material.dart';
// ignore: unused_import
import 'dart:math';
// ignore: unused_import
import 'profil.dart';

// ignore: camel_case_types
// ignore: must_be_immutable
// ignore: camel_case_types
// ignore: must_be_immutable
class Hasil extends StatelessWidget {
// ignore: non_constant_identifier_names
  Hasil({
    // ignore: non_constant_identifier_names
    @required this.panjang_persegi,
    // ignore: non_constant_identifier_names
    @required this.lebar_persegi,
    // ignore: non_constant_identifier_names
  });
// ignore: non_constant_identifier_names
  final int panjang_persegi;
// ignore: non_constant_identifier_names
  final int lebar_persegi;
// ignore: non_constant_identifier_names
  String luas;

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    double luas = (panjang_persegi * lebar_persegi * 1.0);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: Text('Hasil Luas Persegi'),
      ),
      body: Container(
        color: Colors.white,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 20,
              child: Container(
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 10,
              child: Container(
                color: Colors.white,
              ),
            ),
            Text(
              // ignore: unnecessary_brace_in_string_interps
              'Luas Persegi Adalah:',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
            ),
            Text(
              '$luas',
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w400,
                  color: Colors.black),
            ),
            Container(
              //height: double.infinity,
              margin: EdgeInsets.only(left: 10, right: 10, bottom: 20),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith<Color>(
                    (Set<MaterialState> states) {
                      if (states.contains(MaterialState.pressed))
                        return Colors.blue;
                      return Colors.blue; // Use the component's default.
                    },
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/LuasPersegi');
                },
                // textColor: Colors.white,
                child: Text(
                  'Back',
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w500,
                      color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
